import streamlit as st
st.set_page_config(layout="wide")
from os import path
import os
import pathlib
from os import listdir
from os.path import isfile, join
from sdv.tabular import GaussianCopula
from sdv.evaluation import evaluate
#from table_evaluator import TableEvaluator
import time
from hashlib import md5
from time import localtime
import numpy as np
import pandas_profiling
from streamlit_pandas_profiling import st_profile_report

#Performance imports

import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib
import pandas as pd

#session state management
if 'prefix' not in st.session_state:
    st.session_state.prefix = md5(str(localtime()).encode('utf-8')).hexdigest()


#other imports
import sys, os
setupBaseDir = os.path.dirname(__file__)
sys.path.insert(0, setupBaseDir)

model_root = os.path.join(setupBaseDir, "../data/single_table/models/")
real_data_root = os.path.join(setupBaseDir, "../data/single_table/real/")
synthetic_data_root = os.path.join(setupBaseDir, "../data/single_table/synthetic/")
real_dir = os.path.join(setupBaseDir, "../data/single_table/real/")
real_multi_dir = os.path.join(setupBaseDir, "../data/multi_table/real/")
model_TVAE_dir = os.path.join(setupBaseDir, "../data/single_table/models/TVAE/")
model_CopulaGAN_dir = os.path.join(setupBaseDir, "../data/single_table/models/CopulaGAN/")
model_CTGAN_dir = os.path.join(setupBaseDir, "../data/single_table/models/CTGAN/")
model_GaussianCopula_dir = os.path.join(setupBaseDir, "../data/single_table/models/GaussianCopula/")
model_TabularPreset_dir = os.path.join(setupBaseDir, "../data/single_table/models/TabularPreset/")
synthetic_TVAE_dir = os.path.join(setupBaseDir, "../data/single_table/synthetic/TVAE/")
synthetic_CopulaGAN_dir = os.path.join(setupBaseDir, "../data/single_table/synthetic/CopulaGAN/")
synthetic_CTGAN_dir = os.path.join(setupBaseDir, "../data/single_table/synthetic/CTGAN/")
synthetic_GaussianCopula_dir = os.path.join(setupBaseDir, "../data/single_table/synthetic/GaussianCopula/")
synthetic_TabularPreset_dir = os.path.join(setupBaseDir, "../data/single_table/synthetic/TabularPreset/")

#check if files already exist
synthetic_TVAE_path = synthetic_TVAE_dir+st.session_state.prefix+'.csv'
synthetic_CopulaGAN_path = synthetic_CopulaGAN_dir+st.session_state.prefix+'.csv'
synthetic_CTGAN_path = synthetic_CTGAN_dir+st.session_state.prefix+'.csv'
synthetic_TabularPreset_path = synthetic_TabularPreset_dir+st.session_state.prefix+'.csv'
synthetic_GaussianCopula_path = synthetic_GaussianCopula_dir+st.session_state.prefix+'.csv'

data_dirs = [real_dir, real_multi_dir, model_TVAE_dir, model_CopulaGAN_dir, model_CTGAN_dir, model_GaussianCopula_dir, model_TabularPreset_dir,synthetic_TVAE_dir, synthetic_CopulaGAN_dir,synthetic_CTGAN_dir, synthetic_GaussianCopula_dir,synthetic_TabularPreset_dir  ]

for dir in data_dirs:
    if not os.path.exists(dir):
        os.makedirs(dir)

#globals

available_models = []
model_list = ['TVAE', 'CopulaGAN', 'CTGAN', 'GaussianCopula','TabularPreset']

st.write("""
# SYNTHETIC DATA PROFILER
""")
#Begin
parent_path = pathlib.Path(__file__).parent.parent.resolve()
model_path = model_TVAE_dir
onlyfiles = [f for f in listdir(model_path) if isfile(join(model_path, f))]


tab1, tab2, tab3 = st.tabs(['Evaluation','Correlations','Interactions'])
data_type = st.sidebar.radio('Data Format', ('SingleTable', 'MultiTable'))
with tab1:
    if data_type == 'MultiTable':
        st.write('Future Implementation')
    else:
         option6 = st.selectbox('Pick or paste your Model ID (hash) .pkl', onlyfiles)    
         sample_size = st.number_input('Number of rows to evaluate with', 100)
         if st.button('Profile'):
            real_file = real_data_root+option6[:33]+'csv'
            real_data = pd.read_csv(real_file)
            pr = real_data.profile_report()
            st_profile_report(pr)

            with st.spinner('Evaluating...'):
                    time.sleep(5)
            st.success('Done! Proceed to the next tabs to view')
